# Mô hình MVVM

## 1. Giới Thiệu

MVVM là viết tắt của Model, View, ViewModel

- **Model:** Là nơi giữ dữ liệu của ứng dụng, được lấy từ nhiều nguồn khác nhau. Nó không thể nói chuyện trực tiếp với View.

- **View:** Nó đại diện cho giao diện người dùng của ứng dụng không có bất kỳ Logic ứng dụng nào. Nó quan sát ViewModel.

- **ViewModel:** Nó có trách nhiệm chuẩn bị và quản lý dữ liệu, cho một UI component (có thể là Activity hoặc Fragment). Nó cung cấp cách để dễ dàng giao tiếp giữa activity và fragment hoặc giữa các fragment với nhau.

![hihoay](https://images.viblo.asia/full/ae0f036b-01f1-4072-ba34-d166191b209f.png):

#### Cấu trúc dự án

![hioay](https://images.viblo.asia/full/33030ba9-b843-4301-a2b0-fc272e14b1be.png)

#### Set up

Ở build.gradle level app chúng ta hãy thêm vào:

```
    ...
    android {
        ...
        buildFeatures {
        dataBinding = true
        }
    }
```

#### Model

##### Model chứa phần data được lấy từ nhiều nguồn khác nhau, ví dụ như:

- REST API

- Realm db

- SQLite db

- Handles broadcast

- Shared Preferences

- Firebase

- … **Model sẽ chứa toàn bộ business logic, mix giữa các luồng dữ liệu( giữa local data và remote data) trước khi dữ liệu đó được hiển thị cho client. Model còn chứa cái object và các thành phần dữ liệu *

###### Ví dụ

Model User
```
package com.example.demomvvm.data.model

import android.util.Patterns


class User(private val mEmail: String?, private val mPassword: String?) {

    val email: String
        get() = mEmail ?: ""
    val password: String
        get() {
            return mPassword ?: ""
        }
    val isEmailValid: Boolean
        get() = Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()
    val isPasswordLengthGreaterThan5: Boolean
        get() = mPassword!!.length > 5
}

```

#### View : Thành phần giao diện của ứng dụng.

##### View là thành phần duy nhất mà người dùng có thể tương tác được trong chương trình, nó chính là thành phần mô tả dữ liệu. Trong lập trình android, View là một activity, fragment, hay một custom view... Sau đây là một số hành động mà view có thể thực hiện:

- Starting activity

- Lắng nghe sự kiện từ người dùng

- Hiển thị Toast, Dialog , Snackbars.

- …

###### Ví dụ

***activity_main.xml***
```
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_gravity="center"
            android:layout_margin="8dp"
            android:orientation="vertical">

            <com.google.android.material.textfield.TextInputLayout
                android:id="@+id/tl_email"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                app:errorEnabled="true">

                <EditText
                    android:id="@+id/inEmail"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:hint="Email"
                    android:inputType="textEmailAddress"
                    android:padding="8dp" />

            </com.google.android.material.textfield.TextInputLayout>

            <com.google.android.material.textfield.TextInputLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:id="@+id/tl_password"
                app:errorEnabled="true">

                <EditText
                    android:id="@+id/inPassword"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:hint="Password"
                    android:inputType="textPassword"
                    android:padding="8dp"
                     />
            </com.google.android.material.textfield.TextInputLayout>


            <Button
                android:id="@+id/button"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="8dp"

                android:text="LOGIN" />

        </LinearLayout>

    </ScrollView>

</layout>
```

#### View Model

#### ViewModel là một abstraction của View. Nó sẽ lấy dữ liệu từ tầng Model, xửu lý UI logic sau đó hiển thị data có liên quan tới view. ViewModel sẽ không có bất kỳ behavior nào để tương tác với View. Như vậy để nhận biết khi nào cần hiển thị dư liệu, View sẽ đăng ký nhận notification từ ViewModel. Một số chức năng cụ thể mà ViewModel có thể thực hiện :

Truy suất và thông báo hiển thị dữ liệu cho view.

Xử lý visibility của View

Xác thực dữ liệu đầu vào

#### … ViewModel chỉ nên biết về application context. Chúng ta chỉ nên thực hiện một số hành động liên quan đến context tại ViewModel như sau :

Start a service

Bind to a service

Gửi một broadcast

Đăng ký broadcast receiver

Load resource values

### ViewModel không nên:

Hiện thị một dialog

Start activity

Inflate layout

###### Ví dụ ViewModel:

***LoginViewModel.kt***

```
package com.example.demomvvm.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.demomvvm.data.model.User


class LoginViewModel : ViewModel() {
    var errorPassword = MutableLiveData<String?>()
    var errorEmail = MutableLiveData<String?>()

    var email = MutableLiveData<String>()
    var password = MutableLiveData<String>()
    private var userMutableLiveData: MutableLiveData<User>? = null

    fun getUser(): MutableLiveData<User>? {
        if (userMutableLiveData == null) {
            userMutableLiveData = MutableLiveData()
        }
        return userMutableLiveData
    }

    fun onLoginClicked() {
        val user = User(email.value, password.value)
        if (!user.isEmailValid) {
            errorEmail.setValue("Enter a valid email address")
        } else {
            errorEmail.setValue(null)
        }
        if (!user.isPasswordLengthGreaterThan5) errorPassword.setValue("Password Length should be greater than 5") else {
            errorPassword.setValue(null)
        }
       getUser()!!.value = user

    }
}
```

Sử dụng ViewModel: ***MainActivity.kt***

```
package com.example.demomvvm.view
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.example.demomvvm.R
import com.example.demomvvm.databinding.ActivityMainBinding
import com.example.demomvvm.viewmodel.LoginViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        loginViewModel = LoginViewModel()
        binding.button.setOnClickListener {
            loginViewModel.email.value = binding.inEmail.text.toString()
            loginViewModel.password.value = binding.inPassword.text.toString()
            loginViewModel.onLoginClicked()
            Log.e("user",loginViewModel.getUser()!!.value.toString())
            Log.e("mail", loginViewModel.getUser()!!.value!!.email)
            Log.e("pass", loginViewModel.getUser()!!.value!!.password)
            binding.tlEmail.error = loginViewModel.errorEmail.value
            binding.tlPassword.error = loginViewModel.errorPassword.value
        }
    }
}
```

### MVVM + Corotines + API

1. **Tạo Activity**: ***activity_post.xml***

```
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">
    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <androidx.recyclerview.widget.RecyclerView
            android:id="@+id/recyclerview"
            android:layout_width="match_parent"
            android:layout_height="match_parent"/>
    </RelativeLayout>
</layout>
```

2. **Tạo Model**

```
package com.example.demomvvm.data.model

import com.google.gson.annotations.SerializedName

data class Post(
    @SerializedName("link")
     var link: String = "",
    @SerializedName("name")
     val name: String = "",
    @SerializedName("thumb")
     val thumb: String = ""
)
```
3. **Tạo Adapter**

```
package com.example.demomvvm.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.demomvvm.R
import com.example.demomvvm.data.model.Post


class PostAdapter(arrayList: ArrayList<Post>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val arr: ArrayList<Post>



    init {
        arr = arrayList
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return Holder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_post, viewGroup, false)
        )
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val holder = viewHolder as Holder

        val itemTheme: Post = arr[i]
        holder.tvnName.text = itemTheme.name
        holder.tvList.text = itemTheme.name
        Glide.with(holder.gifImageView.context)
            .load(itemTheme.thumb)
            .into(holder.gifImageView)
    }

    override fun getItemCount(): Int {
        return arr.size
    }

    internal inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        var gifImageView: ImageView
        var tvnName: TextView
        var tvList: TextView

        init {
            gifImageView = view.findViewById<View>(R.id.imgThumbnail) as ImageView
            tvnName = view.findViewById<View>(R.id.text_title) as TextView
            tvList = view.findViewById<View>(R.id.text_description) as TextView
        }


    }
}
```

4. **Tạo interface ApiService**

```
package com.example.demomvvm.data.api



import com.example.demomvvm.data.model.Post
import okhttp3.ResponseBody
import retrofit2.Call

import retrofit2.http.GET

interface ApiService {
    @GET("/static/videos.json")
    suspend fun getVideos():  ArrayList<Post>
    @GET("/static/photos.json")
    suspend fun getPhotos():  ArrayList<Post>
}
```

5. **Tạo ViewModel để load API**

```
package com.example.demomvvm.viewmodel

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.demomvvm.data.api.ApiService
import com.example.demomvvm.data.model.Post
import com.example.demomvvm.data.model.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PostViewModel : ViewModel() {

    private var userMutableLiveData: MutableLiveData<ArrayList<Post>>? = null

    fun getAllPost(): MutableLiveData<ArrayList<Post>>? {
        if (userMutableLiveData == null) {
            userMutableLiveData = MutableLiveData()
        }
        return userMutableLiveData
    }


    fun loadAllPost()  {

        
        val listPost: ArrayList<Post> = ArrayList()
        val retrofit = Retrofit.Builder()
            .baseUrl("https://taymay.vn/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val apiService = retrofit.create(ApiService::class.java)

        CoroutineScope(Dispatchers.Main).launch {
            try {

                val response = apiService.getPhotos()
                if (response.isNotEmpty()) {

                    for (item in response) {
                        listPost.add(
                            Post(
                                "https://taymay.vn/" + item.link,
                                item.name,
                                "https://taymay.vn/" + item.thumb
                            )
                        )
                    }

                } else {

                }
                getAllPost()!!.value = listPost
                Log.e("bbbbbb", getAllPost()!!.value.toString())


            } catch (e: Exception) {

            }
        }

    }
}
```

6. **Hiển thị dữ liệu**: ***PostActivity.kt***

```
package com.example.demomvvm.view

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demomvvm.R
import com.example.demomvvm.adapter.PostAdapter
import com.example.demomvvm.data.model.Post
import com.example.demomvvm.databinding.ActivityPostBinding
import com.example.demomvvm.viewmodel.PostViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PostActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPostBinding
    private lateinit var adapter: PostAdapter
    lateinit var postViewModel: PostViewModel
    companion object {
        var lists = ArrayList<Post>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post)

        postViewModel = PostViewModel()
        binding.recyclerview.layoutManager = LinearLayoutManager(this)
        adapter = PostAdapter(lists)
        binding.recyclerview.adapter = adapter
        postViewModel.getAllPost()!!.observe(this) { postList ->

            if (postList.isEmpty()) {
                Log.e("state","loading")
            } else {
                lists = postList
                Log.e("state","done")
                Log.e("list",lists.toString())
                adapter.notifyDataSetChanged()
            }
        }

        
        postViewModel.loadAllPost()

    }
}
```