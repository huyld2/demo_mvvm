package com.example.demomvvm.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.demomvvm.R
import com.example.demomvvm.data.model.Post


class PostAdapter(arrayList: ArrayList<Post>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val arr: ArrayList<Post>



    init {
        arr = arrayList
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        return Holder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_post, viewGroup, false)
        )
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val holder = viewHolder as Holder

        val itemTheme: Post = arr[i]
        holder.tvnName.text = itemTheme.name
        holder.tvList.text = itemTheme.name
        Glide.with(holder.gifImageView.context)
            .load(itemTheme.thumb)
            .into(holder.gifImageView)
    }

    override fun getItemCount(): Int {
        return arr.size
    }

    internal inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        var gifImageView: ImageView
        var tvnName: TextView
        var tvList: TextView

        init {
            gifImageView = view.findViewById<View>(R.id.imgThumbnail) as ImageView
            tvnName = view.findViewById<View>(R.id.text_title) as TextView
            tvList = view.findViewById<View>(R.id.text_description) as TextView
        }


    }
}
