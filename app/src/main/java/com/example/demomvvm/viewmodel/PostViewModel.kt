package com.example.demomvvm.viewmodel

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.demomvvm.data.api.ApiService
import com.example.demomvvm.data.model.Post
import com.example.demomvvm.data.model.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PostViewModel : ViewModel() {

    private var userMutableLiveData: MutableLiveData<ArrayList<Post>>? = null

    fun getAllPost(): MutableLiveData<ArrayList<Post>>? {
        if (userMutableLiveData == null) {
            userMutableLiveData = MutableLiveData()
        }
        return userMutableLiveData
    }


    fun loadAllPost()  {

        
        val listPost: ArrayList<Post> = ArrayList()
        val retrofit = Retrofit.Builder()
            .baseUrl("https://taymay.vn/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val apiService = retrofit.create(ApiService::class.java)

        CoroutineScope(Dispatchers.Main).launch {
            try {

                val response = apiService.getPhotos()
                if (response.isNotEmpty()) {

                    for (item in response) {
                        listPost.add(
                            Post(
                                "https://taymay.vn/" + item.link,
                                item.name,
                                "https://taymay.vn/" + item.thumb
                            )
                        )
                    }

                } else {

                }
                getAllPost()!!.value = listPost
                Log.e("bbbbbb", getAllPost()!!.value.toString())


            } catch (e: Exception) {

            }
        }

    }
}