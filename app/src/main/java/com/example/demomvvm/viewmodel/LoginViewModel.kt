package com.example.demomvvm.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.demomvvm.data.model.User


class LoginViewModel : ViewModel() {
    var errorPassword = MutableLiveData<String?>()
    var errorEmail = MutableLiveData<String?>()

    var email = MutableLiveData<String>()
    var password = MutableLiveData<String>()
    private var userMutableLiveData: MutableLiveData<User>? = null

    fun getUser(): MutableLiveData<User>? {
        if (userMutableLiveData == null) {
            userMutableLiveData = MutableLiveData()
        }
        return userMutableLiveData
    }


    fun onLoginClicked() {


        val user = User(email.value, password.value)
        if (!user.isEmailValid) {
            errorEmail.setValue("Enter a valid email address")
        } else {
            errorEmail.setValue(null)
        }
        if (!user.isPasswordLengthGreaterThan5) errorPassword.setValue("Password Length should be greater than 5") else {
            errorPassword.setValue(null)
        }
       getUser()!!.value = user

    }
}