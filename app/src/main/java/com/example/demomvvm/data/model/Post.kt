package com.example.demomvvm.data.model

import com.google.gson.annotations.SerializedName

data class Post(
    @SerializedName("link")
     var link: String = "",
    @SerializedName("name")
     val name: String = "",
    @SerializedName("thumb")
     val thumb: String = ""
)