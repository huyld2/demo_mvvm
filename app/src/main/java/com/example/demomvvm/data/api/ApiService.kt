package com.example.demomvvm.data.api



import com.example.demomvvm.data.model.Post
import okhttp3.ResponseBody
import retrofit2.Call

import retrofit2.http.GET

interface ApiService {
    @GET("/static/videos.json")
    suspend fun getVideos():  ArrayList<Post>
    @GET("/static/photos.json")
    suspend fun getPhotos():  ArrayList<Post>
}