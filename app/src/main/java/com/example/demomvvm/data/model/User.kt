package com.example.demomvvm.data.model

import android.util.Patterns


class User( val mEmail: String?, val mPassword: String?) {

    val email: String
        get() = mEmail ?: ""
    val password: String
        get() {
            return mPassword ?: ""
        }
    val isEmailValid: Boolean
        get() = Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()
    val isPasswordLengthGreaterThan5: Boolean
        get() = mPassword!!.length > 5
}


