package com.example.demomvvm.view

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demomvvm.R
import com.example.demomvvm.adapter.PostAdapter
import com.example.demomvvm.data.model.Post
import com.example.demomvvm.databinding.ActivityPostBinding
import com.example.demomvvm.viewmodel.LoginViewModel
import com.example.demomvvm.viewmodel.PostViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PostActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPostBinding
    private lateinit var adapter: PostAdapter
    lateinit var postViewModel: PostViewModel

    companion object {
        var lists = ArrayList<Post>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post)
        Log.e("users", MainActivity.loginViewModel.getUser()!!.value!!.email.toString())
        postViewModel = PostViewModel()
        binding.recyclerview.layoutManager = LinearLayoutManager(this)
        adapter = PostAdapter(lists)
        binding.recyclerview.adapter = adapter
        postViewModel.getAllPost()!!.observe(this) { postList ->

            if (postList.isEmpty()) {
                Log.e("state","loading")
            } else {
                lists = postList
                Log.e("state","done")
                Log.e("list",lists.toString())
                adapter.notifyDataSetChanged()
            }
        }


        postViewModel.loadAllPost()

    }
}