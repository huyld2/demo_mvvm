package com.example.demomvvm.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.example.demomvvm.R
import com.example.demomvvm.databinding.ActivityMainBinding
import com.example.demomvvm.viewmodel.LoginViewModel

class MainActivity : AppCompatActivity() {


    private lateinit var binding: ActivityMainBinding

    companion object{
        var loginViewModel: LoginViewModel = LoginViewModel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.button.setOnClickListener {
            loginViewModel.email.value = binding.inEmail.text.toString()
            loginViewModel.password.value = binding.inPassword.text.toString()
            loginViewModel.onLoginClicked()
            Log.e("user",loginViewModel.getUser()!!.value.toString())
            Log.e("mail", loginViewModel.getUser()!!.value!!.email)
            Log.e("pass", loginViewModel.getUser()!!.value!!.password)
            binding.tlEmail.error = loginViewModel.errorEmail.value
            binding.tlPassword.error = loginViewModel.errorPassword.value

            if (loginViewModel.errorEmail.value.isNullOrEmpty()&&loginViewModel.errorPassword.value.isNullOrEmpty()){
                startActivity(Intent(this,PostActivity::class.java))
                finish()
            }
        }
    }
}